import * as functions from 'firebase-functions';
import { PaypageClient, SealCalculator } from '@worldline/sips-payment-sdk';
import { Environment, PaymentRequest } from '@worldline/sips-payment-dom';
const express = require("express");
const cors = require("cors");
const request = require("request");


const app = express();

app.use(cors({ origin: true }));


app.post("/", (req, res) => {
	res.setHeader("Content-Type", "application/json");

	//Allow Api Calls from local server
	  const allowedOrigins = [
	    "http://127.0.0.1:8080",
	    "http://localhost:8080",
	    "https://vote-vite.web.app/"
	  ];
	  const origin = req.headers.origin;
	  if (allowedOrigins.indexOf(origin) > -1) {
	    res.setHeader("Access-Control-Allow-Origin", origin);
	  }

		const amount = req.body.amount;
		const currencyCode = req.body.currencyCode;
		const paymentPattern = req.body.paymentPattern;
		const transactionReference = req.body.transactionReference;
		const interfaceVersion = req.body.interfaceVersion;
		const normalReturnUrl = req.body.normalReturnUrl;
		const paymentMeanBrandList = req.body.paymentMeanBrandList;
		const orderChannel = req.body.orderChannel;
		

		const paypageClient = new PaypageClient(
		  Environment.TEST,
		  '201040027570001', 
		  1, // This shouldn't be hardcoded here...
		  'H6znJWqPj9cTRFe9eT62ulurkb_Lrksbw4PFtMSVb74');

		console.log(paypageClient)


		const paymentRequest = new PaymentRequest();
		    paymentRequest.amount = amount;
		    paymentRequest.currencyCode = currencyCode;
		    paymentRequest.paymentPattern = paymentPattern
		    paymentRequest.transactionReference = transactionReference;
		    paymentRequest.interfaceVersion = interfaceVersion
		    paymentRequest.normalReturnUrl = normalReturnUrl
		    paymentRequest.paymentMeanBrandList = paymentMeanBrandList
		    paymentRequest.orderChannel = orderChannel
		   
		    console.log(paymentRequest);

		const merchantId = '201040027570001'
		const secretKey = 'H6znJWqPj9cTRFe9eT62ulurkb_Lrksbw4PFtMSVb74'
		const keyVersion = "1"

		const requestToMake = paymentRequest;
      		requestToMake.merchantId = merchantId;
      		requestToMake.keyVersion = keyVersion;
      		requestToMake.seal = SealCalculator.calculateSeal(SealCalculator.getSealString(paymentRequest), secretKey);
      		console.log(JSON.stringify(requestToMake))

      	var options = {
		    method: "POST",
		    url: "https://payment-webinit.test.sips-atos.com/rs-services/v2/paymentInit",
		    json: requestToMake
		  };

		  request(options, function(error, response, body) {
		    if (error) throw new Error(error);

		    res.send(body);
		  });

});

exports.payment = functions.https.onRequest(app);

